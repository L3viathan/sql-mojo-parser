# -*- coding: utf-8 -*-

# pylint: disable=invalid-name

import ply.lex as lex
import ply.yacc as yacc


__all__ = ["parse_sql"]


class ParsingError(Exception):
    pass


tokens = (
    "SELECT",
    "FROM",
    "WHERE",
    "ORDER",
    "BY",
    "LIMIT",
    "ASC",
    "DESC",

    "IDENTIFIER",
    "STRING",
    "INTEGER",
    "STAR",

    "EQ",
    "NEQ",
    "GT",
    "GE",
    "LT",
    "LE",

    "AND",
    "OR",
)

literals = (
    "(", ")", ',',
)

t_ignore = "\t"


def t_SELECT(t):
    r"(?i)\bselect\b"
    t.value = t.value.lower()
    return t


def t_FROM(t):
    r"\b(?i)from\b"
    t.value = t.value.lower()
    return t


def t_WHERE(t):
    r"\b(?i)where\b"
    t.value = t.value.lower()
    return t


def t_ORDER(t):
    r"\b(?i)order\b"
    t.value = t.value.lower()
    return t


def t_BY(t):
    r"\b(?i)by\b"
    t.value = t.value.lower()
    return t


def t_ASC(t):
    r"\b(?i)asc\b"
    t.value = t.value.lower()
    return t


def t_DESC(t):
    r"\b(?i)desc\b"
    t.value = t.value.lower()
    return t


def t_LIMIT(t):
    r"\b(?i)limit\b"
    t.value = t.value.lower()
    return t


def t_AND(t):
    r"\b(?i)and\b"
    t.value = t.value.lower()
    return t


def t_OR(t):
    r"\b(?i)or\b"
    t.value = t.value.lower()
    return t


def t_STAR(t):
    r"\*"
    return t


def t_IDENTIFIER(t):
    r"[a-zA-Z_-][0-9a-zA-Z_-]*"
    # Check if value is a reserved keyword, if so adjust the type
    if t.value.upper() in tokens:
        t.type = t.value.upper()
    return t


def t_NEWLINE(t):
    r"\n+"
    t.lexer.lineno += t.value.count("\n")


def t_INTEGER(t):
    r"\d+"
    t.value = int(t.value)
    return t


def t_STRING(t):
    r"'.*?'"
    return t


def t_LE(t):
    r"<="
    return t


def t_GE(t):
    r">="
    return t


def t_EQ(t):
    r"="
    return t


def t_NEQ(t):
    r"(!=|<>)"
    return t


def t_GT(t):
    r">"
    return t


def t_LT(t):
    r"<"
    return t


def t_error(t):
    print("Illegal character '%s'" % t.value[0])
    t.lexer.skip(1)


def p_error(_):
    raise ParsingError("Failed to parse statement")


def p_query(state):
    """query : select_stmt"""
    state[0] = state[1]


# pylint: disable=line-too-long
def p_select_stmt(state):
    """select_stmt : SELECT select_target FROM identifier_list where orderby limit"""
    result = {
        "select": state[2],
        "from": state[4],
    }

    if state[5]:
        result["where"] = state[5]

    if state[6]:
        result["orderby"] = state[6]

    if state[7]:
        result["limit"] = state[7]

    state[0] = result


def p_orderby(state):
    """orderby : empty
               | ORDER BY order_identifier_list
    """
    if len(state) > 2:
        state[0] = state[3]


def p_select_target(state):
    """select_target : STAR
                     | identifier_list
                     | function_call
    """
    if not isinstance(state[1], dict):
        state[1] = {
            "columns": state[1] if state[1] != '*' else [state[1]]
        }
    state[0] = state[1]


def p_function_call(state):
    """function_call : IDENTIFIER '(' identifier_list ')'"""
    state[0] = {
        "function": state[1],
        "args": state[3],
    }


def p_identifier_list(state):
    """identifier_list : IDENTIFIER
                       | identifier_list ',' IDENTIFIER
    """
    if len(state) == 2:
        state[0] = [state[1]]
    else:
        state[0] = state[1] + [state[3]]


def p_order_identifier_list(state):
    """order_identifier_list : IDENTIFIER order_direction
                             | order_identifier_list ',' IDENTIFIER order_direction
    """
    if len(state) == 3:
        state[0] = []
        column = [state[1]]
        direction = state[2]
    else:
        state[0] = state[1]
        column = [state[3]]
        direction = state[4]

    if direction:
        column.append(direction)

    state[0].append(column)


def p_order_direction(state):
    """order_direction : empty
                       | ASC
                       | DESC
    """
    state[0] = state[1]


def p_where(state):
    """where : empty
             | WHERE conditions
    """
    if len(state) == 2:
        state[0] = state[1]
    else:
        state[0] = state[2]


def p_conditions(state):
    """conditions : expr
                  | conditions boolean_operator expr
    """
    # Handle simple case w/o any boolean operators
    if len(state) == 2:
        state[0] = state[1]
        return

    # Handle case after the initial case and construct a dict from the current
    # and last expr with op as the key
    op = state[2]
    if len(state[1]) > 1:
        state[0] = {op: [state[1], state[3]]}
        return

    # Handle consecutive occurences of the same operator
    if op in state[1]:
        state[1][op].append(state[3])
        state[0] = state[1]
        return

    # Handle nested expr due to mixed operator occurences
    if op == "or":
        antiop = "and"
    else:
        antiop = "or"

    last_expr = state[1][antiop][-1]
    if op in last_expr:
        state[1][antiop][-1][op].append(state[3])
    else:
        state[1][antiop][-1] = {op: [last_expr, state[3]]}

    state[0] = state[1]


def p_expr(state):
    """expr : value comparison_operator value"""
    state[0] = {
        "left": state[1],
        "op": state[2],
        "right": state[3],
    }


def p_comparison_operator(state):
    """comparison_operator : EQ
                           | NEQ
                           | GT
                           | GE
                           | LT
                           | LE
    """
    state[0] = state[1]


def p_value(state):
    """value : IDENTIFIER
             | STRING
             | INTEGER
    """
    state[0] = state[1]


def p_boolean_operator(state):
    """boolean_operator : AND
                        | OR
    """
    state[0] = state[1]


def p_limit(state):
    """limit : empty
             | LIMIT INTEGER
    """
    if len(state) == 2:
        state[0] = state[1]
    else:
        state[0] = int(state[2])


def p_empty(_):
    "empty :"
    pass


lexer = lex.lex()
parser = yacc.yacc()
parse_sql = parser.parse
