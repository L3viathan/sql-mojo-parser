#!/usr/bin/env python
# -*- coding: utf-8 -*-

""" Simple REPL which takes SQL statements and outputs the JSON """

import json

from sql_parser import parse_sql


debug = True

def main():
    """ Main loop """
    while True:
        try:
            stmt = input(" > ")
            result = parse_sql(stmt, debug=debug)
            print(json.dumps(result, indent=4))
            if debug:
                print(stmt)
        except (EOFError, KeyboardInterrupt):
            print("Program exit requested by user..")
            break


if __name__ == '__main__':
    main()
