from setuptools import find_packages, setup


setup(
    name='mojo-sql-parser',
    version='0.0.1dev',
    author="amo",
    packages=['sql_parser'],
    license='Creative Commons Attribution-Noncommercial-Share Alike license',
    long_description="Nothing yet.. ",  # open('README.txt').read(),
    install_requires=[
        "ply",
    ],
    test_requires=[
        "pytest",
    ],
)
