# -*- coding: utf-8 -*-

import pytest
import sql_parser as parser


@pytest.mark.parametrize(
    "select_target",
    ("foo", "*"),
)
def test_simple_select(select_target):
    result = parser.parse_sql(
        f"SELECT {select_target} FROM bar"
    )
    assert result == {
        "columns": [select_target],
        "tables": ["bar"],
    }


@pytest.mark.parametrize(
    "op", ("=", "!=", "<>", "<", ">", "<=", ">="),
)
@pytest.mark.parametrize("right", (123, "'test string'"))
def test_select_with_where(op, right):
    result = parser.parse_sql(
        f"SELECT foo FROM bar WHERE baz {op} {right}"
    )

    assert result == {
        "columns": ["foo"],
        "tables": ["bar"],
        "where": {
            "left": "baz",
            "op": op,
            "right": right,
        },
    }
